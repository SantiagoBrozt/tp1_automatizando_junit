package ejercicio_1;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class InscricionTest {
    @Test
    public void inscripcionSimple() {
        //inicialización
        var fechaActual = LocalDate.now();
        var concurso = new Concurso("Rifa municipal", fechaActual.minusDays(1), fechaActual.plusDays(1));
        var participanteFalso = new Participante("Santiago", "Brozt", 42234317);
        //proceso
        System.out.println(concurso.inscribirParticipante("Santiago", "Brozt", 42234317));
        System.out.println( concurso.inscribirParticipante("Santiago", "Brozt", 42234317));

        //verificacion
        assertTrue(concurso.getParticipantes().contains(participanteFalso));
        assertTrue(concurso.getParticipantes().size() == 1);
    }
    @Test
    public void inscripcionPrimerDia(){
        //inicialización
        var fechaActual = LocalDate.now();
        var concurso = new Concurso("Sorteo del club", fechaActual, fechaActual.plusDays(1));
        var participanteFalso = new Participante("Pepe", "Ramirez", 42234318);
        //proceso
        participanteFalso.agregarPuntos(10);
        System.out.println(concurso.inscribirParticipante("Pepe", "Ramirez", 42234318));
        System.out.println(concurso.puntosParticipante(422234317));
        //verificacion
        assertTrue(participanteFalso.getPuntos() == 10);
        assertTrue(Integer.parseInt(concurso.puntosParticipante(participanteFalso.getDni()).get() ) == 10);
        assertTrue(concurso.getParticipantes().contains(participanteFalso));

    }
    @Test
    public void FueraDeTermino(){
        //inicialización
        var fechaActual = LocalDate.now();
        var concursoFuturo = new Concurso("Rifa nacional", fechaActual.plusDays(1), fechaActual.minusDays(2)) ;
        var concursoPasado = new Concurso("Rifa municipal", fechaActual.minusDays(2), fechaActual.minusDays(1)) ;
        var participanteFalso = new Participante("Luca", "Modric", 42234319);
        //proceso
        System.out.println(concursoFuturo.inscribirParticipante("Luca", "Modric", 42234319));
        System.out.println(concursoPasado.inscribirParticipante("Luca", "Modric", 42234319));
        //verificacion
        assertFalse(concursoFuturo.getParticipantes().contains(participanteFalso));
        assertFalse(concursoPasado.getParticipantes().contains(participanteFalso));
    }
}
