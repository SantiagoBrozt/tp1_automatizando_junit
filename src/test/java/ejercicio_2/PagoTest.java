package ejercicio_2;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PagoTest {
	
	@Test
	public void pagoVisa() {
		//inicialización
		Mesa mesa1 = new Mesa(1);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var visa = new Visa(422343168, 1000000);
		//proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        mesa1.cobrar(visa, 0.02);
		//verificación
        assertTrue(visa.verSaldo() == 957430.3);
	}
	
	@Test
	public void pagoMasterCard() {
		//inicialización
		Mesa mesa1 = new Mesa(2);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var masterCard = new MasterCard(422343168, 1000000);
		//proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        mesa1.cobrar(masterCard, 0.02);
		//verificación
        assertTrue(masterCard.verSaldo() == 956996.8);
	}
	
	@Test
	public void pagoComarcaPlus() {
		//inicialización
		Mesa mesa1 = new Mesa(3);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var comarcaPlus = new ComarcaPlus(422343168, 1000000);
		//proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        mesa1.cobrar(comarcaPlus, 0.02);
		//verificación
        assertTrue(comarcaPlus.verSaldo() == 957517);
	}
	
	@Test
	public void pagoTarjetaCualquiera() {
		//inicialización
		Mesa mesa1 = new Mesa(4);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var tarjeta = new Tarjeta(422343168, 1000000);
		//proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        mesa1.cobrar(tarjeta, 0.02);
		//verificación
        assertTrue(tarjeta.verSaldo() == 956650);
	}
	
	@Test
	public void saldoInsuficiente() {
		//inicialización
		Mesa mesa1 = new Mesa(4);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var tarjeta = new Tarjeta(422343168, 9000);
        var visa = new Visa(422343134, 9000);
		//proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        mesa1.cobrar(tarjeta, 0.02);
        System.out.println(tarjeta.verSaldo());
        System.out.println(visa.verSaldo());
		//verificación
        assertTrue(tarjeta.verSaldo() == 9000);
        assertTrue(visa.verSaldo() == 9000);
        
	}
}

