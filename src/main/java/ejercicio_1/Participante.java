package ejercicio_1;

import java.util.Objects;

class Participante {
    private String nombre;
    private String apellido;
    private int dni;
    private int puntos;
    
    Participante(String nombre, String apellido, int dni) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.puntos = 0;
    }
    int getDni(){
        return this.dni;
    }
    public int getPuntos() {
        return this.puntos;
    }
    public void agregarPuntos(int puntos) {
        this.puntos += puntos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participante that = (Participante) o;
        return dni == that.dni && puntos == that.puntos && Objects.equals(nombre, that.nombre) && Objects.equals(apellido, that.apellido);
    }
}
