package ejercicio_1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class Concurso {
    private String nombre;
    private LocalDate fechaApertura;
    private LocalDate fechaCierre;
    private List<Participante> participantes;
    
    Concurso(String nombre, LocalDate apertura, LocalDate cierre){
        this.nombre = nombre;
        this.fechaApertura = apertura;
        this.fechaCierre = cierre;
        this.participantes = new ArrayList<Participante>();
    }
    private boolean estaInscripto(int dni){
        boolean inscripto = false;
        for (Participante participante : this.participantes){
            if (participante.getDni() == dni){
                inscripto = true;
            }
        }
        return inscripto;
    }
    
    List<Participante> getParticipantes(){
        return this.participantes;
    }
    Optional<String> inscribirParticipante(String nombre, String apellido, int dni) {
    	var fechaActual = LocalDate.now();
        if ((!estaInscripto(dni)) && (fechaActual.isAfter(this.fechaApertura.minusDays(1)) && (!fechaActual.isAfter(this.fechaCierre)))) {
            var nuevoParticipante = new Participante(nombre, apellido, dni);
            if (fechaActual.isEqual(this.fechaApertura)) {
                nuevoParticipante.agregarPuntos(10);
            }
            this.participantes.add(nuevoParticipante);
        }
        else {
            return Optional.of("no puede inscribirse, está fuera de fecha.");
        }
        return Optional.empty();
    }

    Optional<String> puntosParticipante(int dni){
        String puntos;
        for (Participante participante : this.participantes){
            if (participante.getDni() == dni){
                puntos = Integer.toString(participante.getPuntos());
                return Optional.of(puntos);
            }
        }
        return Optional.of("no existe un participante con dni " + dni + " en este concurso.");
    }
}
