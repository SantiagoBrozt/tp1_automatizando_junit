package ejercicio_2;

class MasterCard extends Tarjeta {
	private double descuentoBebidas;
	private double descuentoPlatos;

	MasterCard(int numero, double saldo) {
		super(numero, saldo);
		this.descuentoBebidas = 0.0;
		this.descuentoPlatos = 0.02;
	}
	
	double aplicarDescuento(double costoBebidas, double costoPlatos){
		double costoTotal = this.aplicarDescuentoBebidas(costoBebidas) + aplicarDescuentoPlatos(costoPlatos);
		return costoTotal;
	}

	private double aplicarDescuentoBebidas (double costo){
		costo -= costo * this.descuentoBebidas;
		return costo;
	}
	
	private double aplicarDescuentoPlatos (double costo){
		costo -= costo * this.descuentoPlatos;
		return costo;
	}
	
	void pagar(double costoBebidas, double costoPLatos, double propina) throws Exception{
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		super.pagar(costoTotal);
	}
}
