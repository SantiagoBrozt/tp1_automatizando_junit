package ejercicio_2;

class Visa extends Tarjeta {
	private double descuentoBebidas;
	private double descuentoPlatos;

	Visa(int numero, double credito) {
		super(numero, credito);
		this.descuentoBebidas = 0.03;
		this.descuentoPlatos = 0.0;
	}
	
	double aplicarDescuento(double costoBebidas, double costoPlatos){
		Double costoTotal = this.aplicarDescuentoBebidas(costoBebidas) + aplicarDescuentoPlatos(costoPlatos);
		return costoTotal;
	}
	
	private double aplicarDescuentoBebidas (double costo){
		costo -= costo * descuentoBebidas;
		return costo;
	}
	
	private double aplicarDescuentoPlatos (double costo){
		costo -= costo * descuentoPlatos;
		return costo;
	}
	
	void pagar(double costoBebidas, double costoPLatos, double propina) throws Exception{
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		super.pagar(costoTotal);
	}
}
