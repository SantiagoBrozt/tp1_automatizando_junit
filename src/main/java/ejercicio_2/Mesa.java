package ejercicio_2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Mesa {
	private final int numero;
	private static List<Bebida> menuBebidas;
	private static List<PlatoPrincipal> menuPlatos;
	private boolean pedidoConfirmado;
	private List<Bebida> bebidasPedidas;
	private List<PlatoPrincipal> platosPedidos;
	
	
	Mesa(int numero) {
		this.numero = numero;
		this.pedidoConfirmado = false;
		this.bebidasPedidas = new ArrayList<>();
		this.platosPedidos = new ArrayList<>();
	}
	
	List<Bebida> bebidasPedidas(){
		return this.bebidasPedidas;
	}
	
	List<PlatoPrincipal> platosPedidos(){
		return this.platosPedidos;
	}
	
	void pedirBedida(Bebida bebida, int cantidad){
		if (!this.pedidoConfirmado) {
			for(int i = 0; i < cantidad; i++) {
				this.bebidasPedidas.add(bebida);
			}
		}
	}

	void pedirPlato(PlatoPrincipal plato, int cantidad){
		if (!this.pedidoConfirmado) {
			for(int i = 0; i < cantidad; i++) {
				this.platosPedidos.add(plato);
			}	
		}
	}
	void confirmarPedido() {
		this.pedidoConfirmado = true;
	}
	
	private double calcularCostoBebidas() {
		double costoTotal = 0.0;
		for(Bebida bebida : this.bebidasPedidas) {
			costoTotal += bebida.precio();
		}
		return costoTotal;
	}
	
	private double calcularCostoPlatos() {
		double costoTotal = 0.0;
		for(PlatoPrincipal plato : this.platosPedidos) {
			costoTotal += plato.precio();
		}
		return costoTotal;
	}
	
	void cobrar(Tarjeta tarjeta, double propina){
		if(this.pedidoConfirmado) {
			try {
				tarjeta.pagar(this.calcularCostoBebidas(), this.calcularCostoPlatos(), propina);
			} 
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
}
