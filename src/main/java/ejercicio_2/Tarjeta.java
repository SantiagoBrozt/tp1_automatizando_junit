package ejercicio_2;

class Tarjeta {
	private int numero;
	private double saldo;
	
	Tarjeta(int numero, double credito) {
		this.numero = numero;
		this.saldo = credito;
	}
	
	double aplicarDescuento(double costoBebidas, double costoPlatos){
		double costoTotal = costoBebidas + costoPlatos;
		return costoTotal;
	}

	void pagar(double costoBebidas, double costoPLatos, double propina) throws Exception{
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		if(this.saldo >= costoTotal) {
			this.saldo -= costoTotal;			
		}
		else {
			throw new Exception("Saldo insuficiente");
		}
	}
	
	double verSaldo() {
		return this.saldo;
	}
	
	void pagar(double costoTotal) throws Exception {
		if(this.saldo >= costoTotal) {
			this.saldo -= costoTotal;			
		}
		else {
			throw new Exception("Saldo insuficiente");
		}
	}
}
