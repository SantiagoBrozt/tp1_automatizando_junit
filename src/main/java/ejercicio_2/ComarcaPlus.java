package ejercicio_2;

import javax.management.loading.PrivateClassLoader;

class ComarcaPlus extends Tarjeta {
	private double descuentoTotal;

	ComarcaPlus(int numero, double saldo) {
		super(numero, saldo);
		this.descuentoTotal = 0.02;
	}
	
	double aplicarDescuento(double costoBebidas, double costoPlatos){
		double costoTotal = (costoBebidas + costoPlatos);
		costoTotal -= costoTotal * this.descuentoTotal;
		return costoTotal;
	}
	
	void pagar(double costoBebidas, double costoPLatos, double propina) throws Exception{
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		super.pagar(costoTotal);
	}
}
